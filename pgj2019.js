var self = require("./package.json");
var express = require ('express');
const app = express();
var http = require('http').createServer(app);
var net = require('net');
var io = require('socket.io')(http);
var Color = require('color');

var config = require('./config.json');

var socket = undefined;

var names = [
	[
		"Famous",
		"Splendid",
		"Mysterious",
		"Impeccable",
		"Brave",
		"Ambitious",
		"Eager",
		"Fresh",
		"Articulate",
		"Dandy",
		"Glamorous",
		"Grumpy",
		"Gigantic",
		"Massive",
		"Magnificent",
		"Proud",
		"Delightful",
		"Dazzling",
		"Fierce",
		"Square",
		"Bright",
		"Grim",
		"Discombobulated",
		"Finicky",
		"Flummoxed",
		"Befuddled",
		"Doozy",
		"Pauciloquent",
		"Tactical",
		"Piccadilly"
	],
	[
		"Hammer",
		"Labrador",
		"Meerkat",
		"Zebra",
		"Impersonator",
		"Smasher",
		"Devastator",
		"Reaper",
		"Spammer",
		"Doge",
		"Kitteh",
		"Lollygag",
		"Gargoyle",
		"Shenanigans",
		"Squeegee",
		"Maverick",
		"Klutz",
		"Collywobbles",
		"Eliminator",
		"Destructor",
		"Bunny",
		"Catawampus",
		"Troglodyte",
		"Nincompoop",
		"Malarkey",
		"Dummkopf",
		"Pokey-Man",
		"Yaffle",
		"Comedian",
		"Crudivore",
		"Cacophony",
		"Klops",
		"Panjandrum",
		"Rigmarole",
		"Jacuzzi",
		"Dipthong",
		"Poke",
		"Obeliscolychny",
		"Jerboa",
		"Panda",
		"Kangaroo",
		"Octopus",
		"Rhinoceros",
		"Cactus",
		"Jiffy",
		"Chimp",
		"Crook",
		"Boomslang",
		"Klipspringer",
		"Leguan",
		"Antelope",
		"Elk",
		"Aardvark",
		"Dragon",
		"Wildebeest"
	]	
];

function name_gen() {
	
	return "" +
		names[0][Math.floor(Math.random() * names[0].length)] + 
		names[1][Math.floor(Math.random() * names[1].length)];
};


var tcp = net.createServer( (s) => {
	if(typeof socket != 'undefined') {
		socket.end();
	}; 
	socket = s;
	console.log('c ' + socket.remoteAddress);
	
	s.on('data', (b) => {

		if(b.length < 36) return; 
		
		const id = b.slice(0,32).toString().trim();
		const player = players.find(p => p.id == id);

		if(typeof player == 'undefined') {
			console.log("unknown player: " + player);
			return;
		};

		console.log("debug: data for " + player.id);
		
		const pattern_id = b[32];
		var hex_color = "#" + b.slice(33,33+6).toString().trim();
		console.log("debug: hex_color is " + hex_color);
		console.log("debug: player.color is " + player.color);
		console.log("debug: pattern_id is " + pattern_id);
		
		
		//console.log("  " + id + " => " + hex_color + " " + pattern_id);
		
		switch(pattern_id) {
		case 67: // 'C'
			player.color = hex_color;
			var feedback = {
				c: [ 
					{rgb: "#ffffff", t: 50 },
					{rgb: "#000000", t: 50 },
					{rgb: "#ffffff", t: 50 },
					{rgb: hex_color, t: 0 } 
				],
				v: [ 50, 30, 50 ]
			};
			break;
			
		case 68: // 'D
			hex_color = player.color;
			var feedback = {
				c: [ 
					{rgb: "#000000", t: 100 },
					{rgb: hex_color, t: 0 } 
				],
				v: [ 50 ]
			};	
			break;	
		};
	
		console.log("debug: emitting " + feedback);
	
		//console.log("  " + id + " => " + hex_color + " " + pattern_id);
		player.socket.emit('feedback', feedback);
		
	});
	
	closed = () => { socket = undefined; };
	s.on('error', (e) => { console.log('tcp: ' + e.toString())});
	s.on('end', closed);
	s.on('close', closed);
	
	
});
/*
tcp.on('error', (e) => {
	
	socket = undefined;
});*/

tcp.listen(config.tcp_port);
console.log(`TCP server listening on port ` + config.tcp_port)

function tcp_test(ctr) {
	var b = Buffer.alloc(32+32+32);
	
	b.write('dupadupa123' + '\0', 0, 32);
	b.write('\0', 31, 1);
	
	b.write('Player 1' + '\0', 32, 32);
	b.write('\0', 63, 1);
	
	if(ctr % 4 == 0) {
		b.write('r\0\0\0', 64, 4);
	} else {
		b.write('-\0\0\0', 64, 4);
	};
	
	if(typeof socket != 'undefined') {
		socket.write(b);
	};

	setTimeout(() => { tcp_test(++ctr) }, 10);

	
};
//tcp_test(0);


function tcp_send(id, d, n) {
	var b = Buffer.alloc(32+32+32);
	b.fill(32);
	
	b.write(id, 0, 32);
	b.write(n, 32, 32);
	
	b.write(d, 64, 4);
	
	if(typeof socket != 'undefined') {
		socket.write(b);
	};
	
	
	
};

var players = [];

function player_add(id, socket) {
	var player = {
		id: id,
		socket: socket,
		name: name_gen(),
		color: '#FFFFFF',
		queue: []
	};
	players.push(player);
	return player;
};


function player_del(id) {
	players = players.filter((p) => {return p.id != id});	
};


app.use(express.json());

const defaultOptions = {
	root: __dirname + '/static/',
	dotfiles: 'deny',
	headers: {
		'x-timestamp': Date.now(),
		'x-sent': true
	}
};


app.get('/', (req, res) => {
	res.set('Content-Type', 'text/html');
	res.sendFile('control.html', defaultOptions);
});


app.get('/debug', (req, res) => {
	res.set('Content-Type', 'text/html');
	res.send(players);
});

app.get('/feedback/:playerId', (req, res) => {
	res.set('Content-Type', 'text/html');
	const feedback = { 
		c: [
			{rgb: "#000000", t: 150 },
			{rgb: "#ffffff", t: 100 },
			{rgb: "#000000", t: 150 },
			{rgb: "#ffffff", t: 100 },
		],
		v: [ 50, 250, 50 ]
	};

	const playerId = req.params.playerId;
	const player = players.find(p => p.id == playerId);
	player.socket.emit('feedback', feedback);
	
	res.send("");
});

app.get('/playerList', (req, res) => {
	res.set('Content-Type', 'application/json');

	const data = players.map(player => player.id);

	res.send(JSON.stringify(data));
});
/*
app.get('/playerInput', (req, res) => {
	//res.set('Content-Type', 'application/json');

	//const data = JSON.stringify(players);

	//players = players.map(player => ({ player.id, player.queue }));

	//res.send(data);
});
*/
app.get('/p5.min.js', (req, res) => {
	res.set('Content-Type', 'text/javascript');
	res.sendFile('p5.min.js', defaultOptions);
});


io.on('connection', function(socket) {
	
	player = player_add(socket.id, socket);
	
	player.socket.emit('player_name', player.name);
	
	console.log("+ " + socket.id + " (" + players.length + ") " + player.name);

	socket.on('input', function(payload) {
		p = players.find((e) => {return e.id == socket.id});
		p.queue.push(payload);
		
		while(p.queue.length > 1) p.queue.shift();
		tcp_send(socket.id, payload.d, p.name);
		
	});
	
	socket.on('disconnect', function() {
		player = players.find(p => p.id == socket.id);
		console.log("- " + socket.id + " (" + players.length + ") " + player.name);
		tcp_send(socket.id, 'k', player.name);
		player_del(socket.id);
	});
	
});


http.listen(config.http_port, /*'localhost',*/ () => { console.log(`HTTP server listening on port ` + config.http_port); });
